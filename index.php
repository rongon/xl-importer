<?php  
include 'vendor/autoload.php';

$dsn = "mysql:host=localhost;dbname=audit_apotti_db;charset=utf8mb4";
$options = [
    PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array
];
try {
    $pdo = new PDO($dsn, "root", "rongon", $options);
} catch (Exception $e) {
    if ($e->errorInfo[1] === 1062) echo 'Duplicate entry';
}

$xl_filename = 'WORKS 3.xlsx';

$xlsx = SimpleXLSX::parse('sabrina/'.$xl_filename) ;
// $result =  array_slice($xlsx->rows(),3) ;
$result =  $xlsx->rows();
$i = 0;

foreach($result as $data){

    $audit_report_name          = $data[0];
    $nirikkha_dhoron            = $data[1];
    $nirikkhito_ortho_bosor     = $data[2];
    $ministry_division          = $data[3];
    $nirikkhito_office          = $data[4];
    $onucched_no_apotti_title   = $data[5];
    $jorito_ortho_poriman       = $data[6];
    $apotti_status              = $data[7];
    $pa_commitee_meeting        = $data[8];
    $pa_commitee_siddhanto      = $data[9];
    $audit_department_actions   = $data[10];
    $office_ministry_id         = 74;

    if ($pa_commitee_siddhanto == null ) {
        $is_alocito = 0;
    }else{
        $is_alocito = 1;
    }

    $stmt = $pdo->prepare("INSERT INTO reported_apottis (audit_report_name,nirikkha_dhoron,nirikkhito_ortho_bosor,ministry_division,nirikkhito_office,onucched_no_apotti_title,jorito_ortho_poriman,apotti_status,pa_commitee_meeting,pa_commitee_siddhanto,audit_department_actions,is_alocito,xl_filename,office_ministry_id) 
    
    VALUES (:audit_report_name,:nirikkha_dhoron,:nirikkhito_ortho_bosor,:ministry_division,:nirikkhito_office,:onucched_no_apotti_title,:jorito_ortho_poriman,:apotti_status,:pa_commitee_meeting,:pa_commitee_siddhanto,:audit_department_actions,:is_alocito,:xl_filename,:office_ministry_id)");
            $stmt->bindParam('audit_report_name', $audit_report_name);
            $stmt->bindParam('nirikkha_dhoron', $nirikkha_dhoron);
            $stmt->bindParam('nirikkhito_ortho_bosor', $nirikkhito_ortho_bosor);
            $stmt->bindParam('ministry_division', $ministry_division);
            $stmt->bindParam('nirikkhito_office', $nirikkhito_office);
            $stmt->bindParam('onucched_no_apotti_title', $onucched_no_apotti_title);
            $stmt->bindParam('jorito_ortho_poriman', $jorito_ortho_poriman);
            $stmt->bindParam('apotti_status', $apotti_status);
            $stmt->bindParam('pa_commitee_meeting', $pa_commitee_meeting);
            $stmt->bindParam('pa_commitee_siddhanto', $pa_commitee_siddhanto);
            $stmt->bindParam('audit_department_actions', $audit_department_actions);
            $stmt->bindParam('is_alocito', $is_alocito);
            $stmt->bindParam('xl_filename', $xl_filename);
            $stmt->bindParam('office_ministry_id', $office_ministry_id);
            $stmt->execute();

}
?>
<html>
    <table border="1">
        <?php foreach($result as $data): ?>
        <tr>
            <td><?= $i;?></td>
            <td><?= $data[0]; ?></td>
            <td><?= $data[1]; ?></td>
            <td><?= $data[2]; ?></td>
            <td><?= $data[3]; ?></td>
            <td><?= $data[4]; ?></td>
            <td><?= $data[5]; ?></td>
            <td><?= $data[6]; ?></td>
            <td><?= $data[7]; ?></td>
            <td><?= $data[8]; ?></td>
            <td><?= $data[9]; ?></td>
            <td><?= $data[10]; ?></td>
        </tr>
        <?php 
            $i++;
            endforeach;
        ?>
    </table>
</html>
